package pl.rozart.hadoop.lab.rest;

import org.apache.commons.lang.ArrayUtils;
import org.apache.hadoop.fs.Path;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pl.rozart.hadoop.lab.EntropyJob;
import pl.rozart.hadoop.lab.rest.controllers.HadoopRestController;

public class Main {

    private static final Logger LOGGER = LoggerFactory.getLogger(Main.class);

    public static void main(String[] args) {
        if (ArrayUtils.isNotEmpty(args) && args.length == 2) {
            LOGGER.info("Starting Main");
            String inputPathString = args[0];
            String outputPathString = args[1];
            Path inputPath = new Path(inputPathString);
            Path outputPath = new Path(outputPathString);
            EntropyJob entropyJob = new EntropyJob(inputPath, outputPath);
            HadoopRestController hadoopRestController = new HadoopRestController(entropyJob);
        }
    }

}
