package pl.rozart.hadoop.lab.rest.controllers;

import org.apache.commons.lang.StringUtils;
import pl.rozart.hadoop.lab.EntropyJob;
import spark.Spark;

import java.io.IOException;
import java.io.UncheckedIOException;
import java.util.List;
import java.util.Objects;

import static spark.Spark.get;
import static spark.SparkBase.setPort;

public class HadoopRestController {

    public HadoopRestController(EntropyJob entropyJob) {
        setPort(7654);
        get("/run", ((request, response) -> {
            try {
                entropyJob.run();
                return "Entropy job started.";
            } catch (Exception ioe) {
                throw new RuntimeException(ioe);
            }
        }));
        get("/results", ((request, response) -> {
            try {
                List<String> entropyResults = entropyJob.getResults();
                return entropyResults.stream().filter(Objects::nonNull).reduce(StringUtils.EMPTY, (result1, result2) -> result1 + "\n" + result2);
            }catch (IOException ioe){
                throw new UncheckedIOException(ioe);
            }

        }));

    }

}
