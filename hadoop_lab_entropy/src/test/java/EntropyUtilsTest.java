import org.hamcrest.Matchers;
import org.junit.Test;
import pl.rozart.hadoop.lab.utils.EntropyUtils;

import static org.hamcrest.Matchers.is;
import static org.hamcrest.number.IsCloseTo.closeTo;
import static org.junit.Assert.assertThat;

public class EntropyUtilsTest {

    private static final String NUMERIC_PASSWORD = "1234";
    private static final String ALPHA_SAME_CASE_PASSWORD = "bananasy";
    private static final String ALPHA_MIXED_CASE_PASSWORD = "bAnAnAsY";
    private static final String ALPHANUMERIC_PASSWORD = "bAnAnAs1";
    private static final String ALL_CHARS_PASSWORD = "bA*A!As1";

    @Test
    public void testCalculateEntropyAlphanumeric() {
        Double entropy = EntropyUtils.calculateEntropy(ALPHANUMERIC_PASSWORD);
        assertThat(entropy, is(closeTo(47.5, 0.2)));
    }

    @Test
    public void testCalculateEntropyAlpha() {
        Double entropy = EntropyUtils.calculateEntropy(ALPHA_SAME_CASE_PASSWORD);
        assertThat(entropy, is(closeTo(37.5, 0.2)));
    }

    @Test
    public void testCalculateEntropyAlphaMixedCase() {
        Double entropy = EntropyUtils.calculateEntropy(ALPHA_MIXED_CASE_PASSWORD);
        assertThat(entropy, is(Matchers.closeTo(45.5, 0.2)));
    }

    @Test
    public void testCalculateEntropyNumeric() {
        Double entropy = EntropyUtils.calculateEntropy(NUMERIC_PASSWORD);
        assertThat(entropy, is(closeTo(13.2, 0.2)));
    }

    @Test
    public void testCalculateEntropyAllChars(){
        Double entropy = EntropyUtils.calculateEntropy(ALL_CHARS_PASSWORD);
        assertThat(entropy, is(closeTo(52.6, 0.1)));
    }

}
