package pl.rozart.hadoop.lab.reducers;

import org.apache.hadoop.io.DoubleWritable;
import org.apache.hadoop.mapreduce.Reducer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pl.rozart.hadoop.lab.utils.Container;

import java.io.IOException;

public class EntropyReducer extends Reducer<Container, DoubleWritable, Container, DoubleWritable> {

    private static final Logger LOGGER = LoggerFactory.getLogger(EntropyReducer.class);

    @Override
    protected void reduce(Container key, Iterable<DoubleWritable> values, Context context) throws IOException, InterruptedException {
        if (values != null && context != null) {
            for (DoubleWritable doubleWritable : values) {
                context.write(Container.create(key.getKey(), doubleWritable.get()), doubleWritable);
            }
        }
    }
}
