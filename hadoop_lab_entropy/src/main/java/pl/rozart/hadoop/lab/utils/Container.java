package pl.rozart.hadoop.lab.utils;

import org.apache.hadoop.io.WritableComparable;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import java.util.Objects;

public class Container implements WritableComparable<Container> {

    /**
     * The Key.
     */
    private String key;

    /**
     * The Value.
     */
    private Double value;

    public Container() {
        // Empty constructor
    }

    /**
     * Instantiates a new Container.
     *
     * @param key   the key
     * @param value the value
     */
    public Container(String key, Double value) {
        this.key = key;
        this.value = value;
    }

    /**
     * Creates container.
     *
     * @param key   the key
     * @param value the value
     * @return the container
     */
    public static Container create(String key, Double value){
        return new Container(key, value);
    }

    @Override
    public int compareTo(Container o) {
        int compareResult = -1;
        if(o != null) {
            compareResult = Double.compare(this.getValue(), o.getValue());
        }
        return compareResult;
    }

    @Override
    public void write(DataOutput dataOutput) throws IOException {
        dataOutput.writeUTF(this.key);
        dataOutput.writeDouble(this.value);
    }

    @Override
    public void readFields(DataInput dataInput) throws IOException {
        this.key = dataInput.readUTF();
        this.value = EntropyUtils.calculateEntropy(this.key);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Container container = (Container) o;
        return Objects.equals(key, container.key) &&
                Objects.equals(value, container.value);
    }

    @Override
    public int hashCode() {
        return Objects.hash(key);
    }

    @Override
    public String toString() {
        return this.key;
    }

    /**
     * Gets key.
     *
     * @return the key
     */
    public String getKey() {
        return key;
    }

    /**
     * Sets key.
     *
     * @param key the key
     */
    public void setKey(String key) {
        this.key = key;
    }

    /**
     * Gets value.
     *
     * @return the value
     */
    public Double getValue() {
        return value;
    }

    /**
     * Sets value.
     *
     * @param value the value
     */
    public void setValue(Double value) {
        this.value = value;
    }
}
