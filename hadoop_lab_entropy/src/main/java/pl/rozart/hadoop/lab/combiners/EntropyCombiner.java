package pl.rozart.hadoop.lab.combiners;

import org.apache.hadoop.io.DoubleWritable;
import org.apache.hadoop.mapreduce.Reducer;
import pl.rozart.hadoop.lab.utils.Container;

import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

public class EntropyCombiner extends Reducer<Container, DoubleWritable, Container, DoubleWritable> {
    @Override
    protected void reduce(Container key, Iterable<DoubleWritable> values, Context context) throws IOException, InterruptedException {
        Set<Container> uniques = new HashSet<>();
        for (DoubleWritable value : values) {
            Container container = new Container(key.toString().trim(), value.get());
            if (uniques.add(container)) {
                context.write(container, value);
            }
        }
    }
}

