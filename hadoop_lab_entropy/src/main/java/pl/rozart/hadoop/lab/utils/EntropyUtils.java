package pl.rozart.hadoop.lab.utils;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.math3.util.FastMath;
import org.apache.commons.math3.util.MathUtils;
import org.apache.commons.math3.util.Precision;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The Entropy utils.
 */
public class EntropyUtils {

    private static final Logger LOGGER = LoggerFactory.getLogger(EntropyUtils.class);
    /**
     * The constant DIGITS_CHARS_SET.
     */
    private static final int DIGITS_CHARS_SET = 10;
    /**
     * The constant SAME_CASE_LETTERS_CHARS_SET.
     */
    private static final int SAME_CASE_LETTERS_CHARS_SET = 26;
    /**
     * The constant MIXED_CASE_CHARS_SET.
     */
    private static final int MIXED_CASE_CHARS_SET = 52;
    /**
     * The constant MIXED_CASE_WITH_DIGITS_CHARS_SET.
     */
    private static final int MIXED_CASE_WITH_DIGITS_CHARS_SET = 62;
    /**
     * The constant ALL_CHARS_SET.
     */
    private static final int ALL_CHARS_SET = 95;


    /**
     * Calculates password's entropy.
     *
     * @param password the password
     * @return the entropy
     */
    public static Double calculateEntropy(String password) {
        Double entropy = 0.0;
        if (StringUtils.isNotEmpty(password)) {
            entropy = calculateEntropy(password.length(), selectCharSet(password));
        }
        return entropy;
    }

    /**
     * Calculate entropy for password's length.
     *
     * @param passwordLength the length
     * @return the entropy
     */
    private static Double calculateEntropy(int passwordLength, int charSet) {
        Double entropy = (passwordLength * FastMath.log(2.0, charSet));
        entropy = Precision.round(entropy, 2);
        return entropy;
    }

    /**
     * Select char set int.
     *
     * @param password the password
     * @return the int
     */
    private static int selectCharSet(String password) {
        int selectedCharSet = ALL_CHARS_SET;
        if (StringUtils.isNumeric(password)) {
            selectedCharSet = DIGITS_CHARS_SET;
        } else if (StringUtils.isAlpha(password)) {
            if (StringUtils.isAllLowerCase(password) || StringUtils.isAllUpperCase(password)) {
                selectedCharSet = SAME_CASE_LETTERS_CHARS_SET;
            } else {
                selectedCharSet = MIXED_CASE_CHARS_SET;
            }
        } else if (StringUtils.isAlphanumeric(password)) {
            selectedCharSet = MIXED_CASE_WITH_DIGITS_CHARS_SET;
        }
        return selectedCharSet;
    }

}
