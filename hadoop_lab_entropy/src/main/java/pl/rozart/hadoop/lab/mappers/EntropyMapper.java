package pl.rozart.hadoop.lab.mappers;

import org.apache.hadoop.io.DoubleWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pl.rozart.hadoop.lab.utils.Container;
import pl.rozart.hadoop.lab.utils.EntropyUtils;

import java.io.IOException;

public class EntropyMapper extends Mapper<Object, Text, Container, DoubleWritable> {

    private static final Logger LOGGER = LoggerFactory.getLogger(EntropyMapper.class);

    @Override
    protected void map(Object key, Text value, Context context) throws IOException, InterruptedException {
        if (value != null && context != null) {
            String password = value.toString();
            Double passwordEntropy = EntropyUtils.calculateEntropy(password);
            context.write(Container.create(password, passwordEntropy), new DoubleWritable(passwordEntropy));
        }
    }
}
