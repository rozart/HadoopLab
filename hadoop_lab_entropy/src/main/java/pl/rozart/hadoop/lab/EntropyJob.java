package pl.rozart.hadoop.lab;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileStatus;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.DoubleWritable;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.mortbay.log.Log;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pl.rozart.hadoop.lab.combiners.EntropyCombiner;
import pl.rozart.hadoop.lab.mappers.EntropyMapper;
import pl.rozart.hadoop.lab.reducers.EntropyReducer;
import pl.rozart.hadoop.lab.utils.Container;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;


/**
 * The type Entropy job.
 *
 * @author rozart <rozart@gmail.com>
 */
public class EntropyJob {

    private static final Logger LOGGER = LoggerFactory.getLogger(EntropyJob.class);
    /**
     * The constant HDFS_URL.
     */
    private static final String HDFS_URL = "hdfs://localhost:9000";

    /**
     * HDFS input path.
     */
    private Path inputPath;

    /**
     * HDFS outputPath.
     */
    private Path outputPath;

    public EntropyJob(Path inputPath, Path outputPath) {
        LOGGER.info("Creating job");
        this.inputPath = inputPath;
        this.outputPath = outputPath;
    }

    /**
     * Run.
     *
     * @throws Exception the exception
     */
    public void run() throws Exception {
        Configuration configuration = createConfiguration();

        Job job = Job.getInstance(configuration, "entropy");
        job.setOutputKeyClass(Container.class);
        job.setOutputValueClass(DoubleWritable.class);
        job.setMapperClass(EntropyMapper.class);
        job.setCombinerClass(EntropyCombiner.class);
        job.setReducerClass(EntropyReducer.class);
        job.setNumReduceTasks(1);

        FileSystem fs = FileSystem.get(configuration);
        fs.delete(outputPath, true);

        FileInputFormat.setInputPaths(job, inputPath);
        FileOutputFormat.setOutputPath(job, outputPath);
        job.setJarByClass(EntropyJob.class);
        LOGGER.info(job.waitForCompletion(true) ? "success" : "fail");
    }

    public List<String> getResults() throws IOException {
        LOGGER.info("Getting the results.");
        List<String> results = new ArrayList<>();
        FileSystem fileSystem = FileSystem.get(createConfiguration());
        FileStatus[] fileStatuses = fileSystem.listStatus(outputPath);
        for (FileStatus fileStatus : fileStatuses) {
            InputStreamReader inputStreamReader = new InputStreamReader(fileSystem.open(fileStatus.getPath()));
            BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
            String line = bufferedReader.readLine();
            while (line != null) {
                results.add(line);
                line = bufferedReader.readLine();
            }
            results.add(line);
            LOGGER.info("Got " + results.size() + " results.");
        }
        return results;
    }

    private Configuration createConfiguration() {
        LOGGER.info("Creating configuration");
        Configuration configuration = new Configuration();
        configuration.set("fs.default.name", HDFS_URL);
        return configuration;
    }

}
